# Merge Requests' stats normalizer

Apps Script application to normalize Merge Requests statistics, relying on a
[Google Spreadsheet](https://www.google.com/sheets/about/) for I/O operations.

[![pipeline
status](https://gitlab.com/ricardomendes/merge-requests-stats-normalizer/badges/master/pipeline.svg)](https://gitlab.com/ricardomendes/merge-requests-stats-normalizer/-/commits/master)

**Sidenote:** [some values are hard-coded](src/models.ts) to fit my specific
needs, but can be easily changed.

## I/O data format

### Input

The `RawDataReader` [class](src/raw-data-reader.ts) reads data from a sheet
containing raw input data in the format below:

| Column            | Format or Valid values                                          | Mandatory? |
| ----------------- | --------------------------------------------------------------- | :--------: |
| Timestamp         | Date                                                            |    Yes     |
| Status            | 1. Open / 2. Merged / 3. Closed                                 |    Yes     |
| Status date       | Date                                                            |    Yes     |
| MR ID             | number                                                          |    Yes     |
| Component/Project | 1. Rails Server / 2. Runner (including Custom Executor drivers) |    Yes     |
| User email        | string (email)                                                  |    Yes     |

The script currently looks for a sheet with the name `Form Responses 1` in
the active spreadsheet. It has been used in an MVP that receives raw input
through [Google Forms](https://www.google.com/forms/about/).

### Output

The `NormalizedDataPersister` [class](src/normalized-data-persister.ts) writes
into a sheet in the format below:

| Column        | Format or Valid values | Mandatory? |
| ------------- | ---------------------- | :--------: |
| Component     | Rails Server / Runner  |    Yes     |
| MR ID         | number                 |    Yes     |
| Opened at     | Date                   |    Yes     |
| Opened by     | string (email)         |    Yes     |
| Status        | Open / Merged / Closed |    Yes     |
| Merged at     | Date                   |     No     |
| Days to merge | number                 |     No     |
| Closed at     | Date                   |     No     |
| Finished by   | string (email)         |     No     |

The script currently writes into a sheet with the name `Normalized data` in the
active spreadsheet.

## `clasp` setup

Developers need to set up a `.clasp.json` file before pushing this code as a
Google Apps Script:

```bash
cp .clasp.json.sample .clasp.json
clasp setting scriptId <YOUR-GOOGLE-APPS-SCRIPT-ID>
```

_The `.clasp.json` file is not pushed to the Git repository for security
reasons._
