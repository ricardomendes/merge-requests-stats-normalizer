import { DataNormalizer } from "./data-normalizer";
import { NormalizedEntries, RawEntries } from "./models";
import { NormalizedDataPersister } from "./normalized-data-persister";
import { RawDataReader } from "./raw-data-reader";

export function main(): void {
  const raw: RawEntries = new RawDataReader().read();
  const normalized: NormalizedEntries = new DataNormalizer().normalize(raw);
  new NormalizedDataPersister().persist(normalized);
}
