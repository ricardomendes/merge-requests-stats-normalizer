import {
  NormalizedEntries,
  NormalizedEntry,
  RawEntries,
  RawEntry,
  Status,
} from "./models";

export class DataNormalizer {
  private normalizedEntriesMap: Record<string, NormalizedEntry> = {};

  normalize(rawEntries: RawEntries): NormalizedEntries {
    rawEntries.forEach((rawEntry) => {
      this.updateMap(rawEntry);
    });

    const normalizedEntries: NormalizedEntries = [];
    for (const key in this.normalizedEntriesMap) {
      normalizedEntries.push(this.normalizedEntriesMap[key]);
    }

    return normalizedEntries;
  }

  private updateMap(rawEntry: RawEntry): void {
    const key = this.makeMapKey(rawEntry);

    if (!key) {
      return;
    }

    const normalizedEntry = this.normalizedEntriesMap[key];
    if (!normalizedEntry) {
      this.normalizedEntriesMap[key] = this.createNormalizedEntry(rawEntry);
    } else {
      this.updateNormalizedEntry(normalizedEntry, rawEntry);
    }
  }

  private makeMapKey(rawEntry: RawEntry): string {
    return rawEntry.mergeRequestComponent && rawEntry.mergeRequestId
      ? `${rawEntry.mergeRequestComponent}!${rawEntry.mergeRequestId}`
      : null;
  }

  private createNormalizedEntry(rawEntry: RawEntry): NormalizedEntry {
    return {
      mergeRequestComponent: rawEntry.mergeRequestComponent,
      mergeRequestId: rawEntry.mergeRequestId,
      createdAt: rawEntry.date,
      createdBy: rawEntry.userEmail,
      status: rawEntry.status,
      mergedAt: null,
      daysToMerge: null,
      closedAt: null,
      finishedBy: null,
    };
  }

  private updateNormalizedEntry(
    normalizedEntry: NormalizedEntry,
    rawEntry: RawEntry
  ): void {
    normalizedEntry.status = rawEntry.status;

    if (rawEntry.status === Status.Merged) {
      normalizedEntry.mergedAt = rawEntry.date;
      normalizedEntry.daysToMerge = this.calcDaysDiff(
        normalizedEntry.mergedAt,
        normalizedEntry.createdAt
      );
    } else if (rawEntry.status === Status.Closed) {
      normalizedEntry.closedAt = rawEntry.date;
    }

    if (normalizedEntry.mergedAt || normalizedEntry.closedAt) {
      normalizedEntry.finishedBy = rawEntry.userEmail;
    }
  }

  private calcDaysDiff(date1: Date, date2: Date): number {
    const diff = Math.abs(date1.getTime() - date2.getTime());
    return Math.ceil(diff / (1000 * 3600 * 24));
  }
}
