/* eslint-disable  @typescript-eslint/no-unused-vars */
import { main } from "./index";

export function onOpen(): void {
  // Add a custom menu to the spreadsheet.
  SpreadsheetApp.getUi()
    .createMenu("Data++")
    .addItem("Normalize", "main")
    .addToUi();
}
