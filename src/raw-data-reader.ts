import { Component, RawEntry, RawEntries, Status } from "./models";

export class RawDataReader {
  private static readonly SHEET_NAME = "Form Responses 1";

  read(): RawEntries {
    const activeSpreadsheet = SpreadsheetApp.getActiveSpreadsheet();
    const rawInputSheet = activeSpreadsheet.getSheetByName(
      RawDataReader.SHEET_NAME
    );
    const values: any[][] = rawInputSheet.getDataRange().getValues();

    return values.map((line) => this.lineToRawEntry(line));
  }

  private lineToRawEntry(line: any[]): RawEntry {
    return {
      timestamp: new Date(line[0]),
      status: this.parseStatus(line[1]),
      date: new Date(line[2]),
      mergeRequestId: +line[3],
      mergeRequestComponent: this.parseComponent(line[4]),
      userEmail: line[5],
    };
  }

  private parseStatus(string: string): Status {
    const statuses: Record<number, Status> = {
      1: Status.Open,
      2: Status.Merged,
      3: Status.Closed,
    };
    return statuses[+string.substring(0, string.indexOf("."))];
  }

  private parseComponent(string: string): Component {
    const components: Record<number, Component> = {
      1: Component.RailsServer,
      2: Component.Runner,
    };
    return components[+string.substring(0, string.indexOf("."))];
  }
}
