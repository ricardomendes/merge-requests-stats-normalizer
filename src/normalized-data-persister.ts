import { NormalizedEntries, NormalizedEntry } from "./models";

export class NormalizedDataPersister {
  private static readonly SHEET_NAME = "Normalized data";

  persist(normalizedEntries: NormalizedEntries): void {
    const activeSpreadsheet = SpreadsheetApp.getActiveSpreadsheet();

    let normalizedSheet = activeSpreadsheet.getSheetByName(
      NormalizedDataPersister.SHEET_NAME
    );

    if (normalizedSheet) {
      normalizedSheet.clear();
    } else {
      normalizedSheet = activeSpreadsheet.insertSheet(
        NormalizedDataPersister.SHEET_NAME
      );
    }

    normalizedSheet.appendRow(this.makeHeaders());
    normalizedEntries.forEach((normalizedEntry) => {
      normalizedSheet.appendRow(this.normalizedEntryToArray(normalizedEntry));
    });
  }

  private makeHeaders(): string[] {
    return [
      "Component",
      "MR ID",
      "Opened at",
      "Opened by",
      "Status",
      "Merged at",
      "Days to merge",
      "Closed at",
      "Finished by",
    ];
  }

  private normalizedEntryToArray(normalizedEntry: NormalizedEntry): any[] {
    return [
      normalizedEntry.mergeRequestComponent,
      normalizedEntry.mergeRequestId,
      normalizedEntry.createdAt,
      normalizedEntry.createdBy,
      normalizedEntry.status,
      normalizedEntry.mergedAt,
      normalizedEntry.daysToMerge,
      normalizedEntry.closedAt,
      normalizedEntry.finishedBy,
    ];
  }
}
