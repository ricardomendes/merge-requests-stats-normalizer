export enum Component {
  RailsServer = "Rails Server",
  Runner = "Runner",
}

export enum Status {
  Open = "Open",
  Merged = "Merged",
  Closed = "Closed",
}

export interface RawEntry {
  timestamp: Date;
  status: Status;
  date: Date;
  mergeRequestId: number;
  mergeRequestComponent: Component;
  userEmail: string;
}

export type RawEntries = RawEntry[];

export interface NormalizedEntry {
  mergeRequestComponent: Component;
  mergeRequestId: number;
  createdAt: Date;
  createdBy: string;
  status: Status;
  mergedAt: Date;
  daysToMerge: number;
  closedAt: Date;
  finishedBy: string;
}

export type NormalizedEntries = NormalizedEntry[];
