import { deepStrictEqual, strictEqual } from "assert";

import {
  Component,
  NormalizedEntries,
  NormalizedEntry,
  RawEntries,
  Status,
} from "../src/models";
import { DataNormalizer } from "../src/data-normalizer";

describe("DataNormalizer class", () => {
  describe("normalize method", () => {
    it("transforms an open MR raw entry into a normalized entry", () => {
      const rawEntries: RawEntries = [
        {
          timestamp: new Date("2020-05-14T13:39:00"),
          status: Status.Open,
          date: new Date("2020-05-14"),
          mergeRequestId: 1234,
          mergeRequestComponent: Component.Runner,
          userEmail: "someone@test",
        },
      ];

      const normalizer: DataNormalizer = new DataNormalizer();
      const normalizedEntries: NormalizedEntries = normalizer.normalize(
        rawEntries
      );

      strictEqual(1, normalizedEntries.length);

      const normalizedEntry: NormalizedEntry = normalizedEntries[0];

      strictEqual(1234, normalizedEntry.mergeRequestId);
      deepStrictEqual(new Date("2020-05-14"), normalizedEntry.createdAt);
      strictEqual(Status.Open, normalizedEntry.status);
      strictEqual(null, normalizedEntry.mergedAt);
      strictEqual(null, normalizedEntry.closedAt);
      strictEqual(null, normalizedEntry.finishedBy);
    });

    it("combines two closed MR related raw entries into a single normalized entry", () => {
      const rawEntries: RawEntries = [
        {
          timestamp: new Date("2020-05-14T13:39:00"),
          status: Status.Open,
          date: new Date("2020-05-14"),
          mergeRequestId: 1234,
          mergeRequestComponent: Component.Runner,
          userEmail: "someone@test",
        },
        {
          timestamp: new Date("2020-05-18T11:20:00"),
          status: Status.Closed,
          date: new Date("2020-05-18"),
          mergeRequestId: 1234,
          mergeRequestComponent: Component.Runner,
          userEmail: "someone@test",
        },
      ];

      const normalizer: DataNormalizer = new DataNormalizer();
      const normalizedEntries: NormalizedEntries = normalizer.normalize(
        rawEntries
      );

      strictEqual(1, normalizedEntries.length);

      const normalizedEntry: NormalizedEntry = normalizedEntries[0];

      strictEqual(1234, normalizedEntry.mergeRequestId);
      deepStrictEqual(new Date("2020-05-14"), normalizedEntry.createdAt);
      strictEqual(Status.Closed, normalizedEntry.status);
      deepStrictEqual(null, normalizedEntry.mergedAt);
      strictEqual(null, normalizedEntry.daysToMerge);
      deepStrictEqual(new Date("2020-05-18"), normalizedEntry.closedAt);
      strictEqual("someone@test", normalizedEntry.finishedBy);
    });

    it("combines two merged MR related raw entries into a single normalized entry", () => {
      const rawEntries: RawEntries = [
        {
          timestamp: new Date("2020-05-14T13:39:00"),
          status: Status.Open,
          date: new Date("2020-05-14"),
          mergeRequestId: 1234,
          mergeRequestComponent: Component.Runner,
          userEmail: "someone@test",
        },
        {
          timestamp: new Date("2020-05-17T17:20:00"),
          status: Status.Merged,
          date: new Date("2020-05-17"),
          mergeRequestId: 1234,
          mergeRequestComponent: Component.Runner,
          userEmail: "someone@test",
        },
      ];

      const normalizer: DataNormalizer = new DataNormalizer();
      const normalizedEntries: NormalizedEntries = normalizer.normalize(
        rawEntries
      );

      strictEqual(1, normalizedEntries.length);

      const normalizedEntry: NormalizedEntry = normalizedEntries[0];

      strictEqual(1234, normalizedEntry.mergeRequestId);
      deepStrictEqual(new Date("2020-05-14"), normalizedEntry.createdAt);
      strictEqual(Status.Merged, normalizedEntry.status);
      deepStrictEqual(new Date("2020-05-17"), normalizedEntry.mergedAt);
      strictEqual(3, normalizedEntry.daysToMerge);
      strictEqual(null, normalizedEntry.closedAt);
      strictEqual("someone@test", normalizedEntry.finishedBy);
    });

    it("ignores non-identifying raw entries", () => {
      const rawEntries: RawEntries = [
        {
          timestamp: new Date("2020-05-14T13:39:00"),
          status: Status.Open,
          date: new Date("2020-05-14"),
          mergeRequestId: null,
          mergeRequestComponent: Component.Runner,
          userEmail: "someone@test",
        },
      ];

      const normalizer: DataNormalizer = new DataNormalizer();
      const normalizedEntries: NormalizedEntries = normalizer.normalize(
        rawEntries
      );

      strictEqual(0, normalizedEntries.length);
    });
  });
});
